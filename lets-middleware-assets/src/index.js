import utils from './lib/utils';
import path from 'path';
import getDefaultConfig from './lib/getDefaultConfig';
import getMaps from './lib/getMaps';
import getContent from './lib/getContent';

// app.use(assets(config));
export default (opts) => {
  getDefaultConfig().then((defaultConfig) => {
    opts = Object.assign({}, defaultConfig, opts || {});
  });
  return function*(next) {
    const context = this;
    let urls = context.urls;
    const extName = utils.getExtName(urls[0]);
    context.needSourceMap = opts.sourcemap && extName === '.js';
    context.isText = utils.isTextFile(extName);
    if (urls && urls.length) {
      let maps = yield getMaps(urls, context.host, opts);
      if (maps) {
        let results = yield maps.map((mapItem, idx) => getContent(mapItem, context, idx, opts));
        context.contents = results.map(result => result.content);
        context.needSourceMap && (context.sourceMaps = results.map(result => result.sourceMap));
      }
    }
    yield next;
  };
};
