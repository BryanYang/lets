import {
  convertToStr
} from './utils';
import getLocalContent from './getLocalContent';
import getUrlContent from './getUrlContent';
import throwNotFound from './throwNotFound';

function getPathList(map, useSrc) {
  let pathList = ['buildpath', 'localpath'];
  useSrc && pathList.unshift('srcpath');
  return pathList.filter(key => !!map[key]).map(key => map[key]);
}

export default async(mapItem, context, idx, opts) => {
  let content = '';
  let result;
  try {
    if (mapItem.type === 'remote') {
      result = await getUrlContent(mapItem.urlList, context.host, !opts.silent, context.needSourceMap);
    } else {
      result = await getLocalContent(getPathList(mapItem, opts.useSrc), !opts.silent, context.needSourceMap);
      if (result.content === undefined) {
        result = await getUrlContent(mapItem.urlList, context.host, !opts.silent, context.needSourceMap);
      }
    }
  } catch (e) {
    return throwNotFound(mapItem, context);
  }
  if (typeof result.content === 'null' || typeof result.content === 'undefined') return throwNotFound(mapItem, context);
  let inputCharset = mapItem.inputCharset = mapItem.inputCharset || 'utf-8';
  if (context.isText) {
    try {
      result.content = convertToStr(result.content, inputCharset);
      result.sourceMap && (result.sourceMap = convertToStr(result.sourceMap, inputCharset));
    } catch (e) {
      throw new Error(mapItem.urlList.join(' ') + ' Convert charset fail!');
    }
  }
  return result;
};
