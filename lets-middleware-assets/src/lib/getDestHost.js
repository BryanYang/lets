import {
  isValidHost,
  dnsResolve
} from './utils';

export default async(hostItem, hostMap) => {
  for (let k in hostMap) {
    if (hostItem.indexOf(k) !== -1) {
      return hostMap[k];
    }
  }
  if (isValidHost(hostItem)) return hostItem;
  try {
    let ips = await dnsResolve(hostItem);
    return ips && ips.length ? ips[0] : null;
  } catch (e) {
    return null;
  }
};
