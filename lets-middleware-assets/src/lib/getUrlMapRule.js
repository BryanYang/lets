export default async(path, ruleList) => {
  let mapRule;
  for (let i = 0, len = ruleList.length; i < len; i++) {
    let urlItem = ruleList[i];
    let rule = urlItem.rule;
    if (rule instanceof RegExp && rule.test(path) || typeof rule === 'string' && path.indexOf(rule) !== -1) {
      mapRule = urlItem;
      break;
    }
  }
  return mapRule;
};
