import { 
  resolve,
  basename 
} from 'path';
import {
  trace,
  getFileStat,
  readFile
} from './utils';

async function getLocalSourceMap(jsPath) {
  const sourceMapName = `${basename(jsPath)}.map`;
  const sourceMapPath = resolve(jsPath, '..', sourceMapName);
  return await readFile(sourceMapPath);
}

async function checkAndReadFile(filePath) {
  let exist;
  let content;
  try {
    let stats = await getFileStat(filePath);
    if (stats && stats.isFile()) {
      content = await readFile(filePath);
      exist = true;
    }
  } catch (e) {
    console.err(e);
  }
  return {
    exist,
    content
  };
}

export default async(pathList, printTrace, needSourceMap) => {
  let content, sourceMap;
  for (let i = 0; i < pathList.length; i++) {
    if (printTrace) trace(pathList[i]);
    let result = await checkAndReadFile(pathList[i]);
    if (!result.exist) continue;
    if (needSourceMap) {
      sourceMap = await getLocalSourceMap(pathList[i]);
    }
    return {
      content: result.content,
      sourceMap
    };
  }
};
