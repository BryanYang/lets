export default (map, context) => {
  let searchList = map.urlList ? map.urlList.concat() : [];
  map.localpath && searchList.unshift(map.localpath);
  map.buildpath && searchList.unshift(map.buildpath);
  map.srcpath && searchList.unshift(map.srcpath);
  context.throw(404, `Not Found\r\nTrace:\r\n${searchList.join('\r\n')}`);
};
