import urllib from 'urllib';
import {
  trace
} from './utils';
import {
  basename
} from 'path';
import url from 'url';

function request(url, oriHost) {
  return new Promise((resolve) => {
    urllib.request(url, {
      headers: {
        host: oriHost
      }
    }, (err, data, res) => {
      resolve({
        statusCode: res && !err ? res.statusCode : 404,
        data
      });
    });
  });
}

async function getRemoteSourceMap(jsUrl, oriHost) {
  const sourceMapName = `${basename(url.parse(jsUrl).pathname)}.map`;
  const sourceMapUrl = url.resolve(jsUrl, sourceMapName);
  await request(sourceMapUrl, oriHost);
}

export default async(urlList, oriHost, printTrace, needSourceMap) => {
  let result, sourceMap;
  for (let i = 0; i < urlList.length; i++) {
    if (printTrace) trace(urlList[i]);
    let ret = await request(urlList[i], oriHost);
    if (ret && ret.statusCode === 200) {
      needSourceMap && (sourceMap = await getRemoteSourceMap(urlList[i], oriHost));
      result = ret;
      break;
    }
  }
  if (!result) {
    let err = new Error(urlList.join(' ') + ' Not Found!');
    throw err;
  } else {
    return {
      content: result.data || '',
      sourceMap: sourceMap && sourceMap.data
    };
  }
};
