import path from 'path';
import {
  isValidHost,
  dnsResolve
} from './utils';

async function getHostList(hostItem, hostMap) {
  let hostList = [];
  for (let k in hostMap) {
    if (hostItem.indexOf(k) !== -1) {
      hostList.push(hostMap[k]);
    }
  }
  if (isValidHost(hostItem)) hostList.push(hostItem);
  try {
    let ips = await dnsResolve(hostItem);
    ips && ips.length && hostList.push(ips[0]);
  } catch (e) {
    console.err(e);
  }
  return hostList;
}

function joinUrl(hostItem, pathItem) {
  let subUrl = path.join(hostItem, pathItem);
  let url = hostItem.startsWith('http') ? subUrl : 'http://' + subUrl;
  return url;
}

export default async(pathItem, oriHost, hostMap) => {
  let hostList = await getHostList(oriHost, hostMap);
  let urlList = hostList.map(hostItem => joinUrl(hostItem, pathItem));
  return urlList;
};
