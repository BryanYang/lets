import path from 'path';
import {
  getAbsolutePath,
  getFileStat
} from './utils';

export default async(pathItem, mapRule) => {
  let pathMap = {};
  let rule = mapRule.rule;
  let matches = pathItem.match(rule);
  let match = matches[0];
  let replaceDest = match.replace(rule, mapRule.dest);
  let dirPath = getAbsolutePath(path.join(replaceDest));
  let fileStat = await getFileStat(dirPath);
  if (fileStat) {
    if (fileStat.isFile()) {
      pathMap.localpath = dirPath;
    } else {
      let seps = pathItem.split(rule);
      let filePath = '';
      for (let i = matches.length, len = seps.length; i < len; i++) {
        let sep = seps[i];
        filePath += !sep.startsWith('/') ? '/' + sep : sep;
      }
      pathMap.localpath = path.join(dirPath, filePath);
      pathMap.srcpath = path.join(dirPath, 'src', filePath);
      pathMap.buildpath = path.join(dirPath, 'build', filePath);
    }
  }
  return pathMap;
};
