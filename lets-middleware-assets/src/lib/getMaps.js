import getRemoteUrl from './getRemoteUrl';
import getUrlMapRule from './getUrlMapRule';
import getLocalPath from './getLocalPath';

// let urls = context.urls;
// let maps = yield getMaps(urls, context.host, opts);
export default async(pathList, host, opts) => {
  let mapArr = [];
  for (let i = 0, len = pathList.length; i < len; i++) {
    let pathItem = pathList[i];
    let urlList = await getRemoteUrl(pathItem, host, opts.hosts);
    // 正则获取url的本地路径
    let mapRule = await getUrlMapRule(pathItem, opts.urls);
    let pathMap = {};
    if (mapRule) pathMap = await getLocalPath(pathItem, mapRule);
    let map = {
      urlList,
      url: pathItem,
      localpath: pathMap.localpath,
      srcpath: pathMap.srcpath,
      buildpath: pathMap.buildpath,
      type: Object.keys(pathMap).length ? 'local' : 'remote'
    };
    mapArr.push(map);
  }
  return mapArr;
};
