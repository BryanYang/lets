import dns from 'dns';
import path from 'path';
import iconv from 'iconv-lite';
import fs from 'fs';

function dnsResolve(host) {
  return new Promise((resolve) => {
    dns.resolve(host, (err, data) => {
      if (err) resolve();
      else resolve(data);
    });
  });
}

function getExtName(filePath) {
  let extName = '';
  try {
    extName = path.extname(filePath);
  } catch (e) {
    console.err(e);
  }
  return extName;
}

function comboAble(extName) {
  const extArr = ['.js', '.css'];
  return extArr.indexOf(extName) !== -1;
}

function isTextFile(extName) {
  const extArr = ['.js', '.css', '.html', '.txt', '.json', '.less', '.map'];
  return extArr.indexOf(extName) !== -1;
}

function isValidHost(str) {
  return /^[\d\.\:]+$/.test(str);
}

function getAbsolutePath(relpath) {
  let homePath = process.env.HOME;
  return homePath ? relpath.replace(/^~/, homePath) : relpath;
}

function trace(info) {
  console.log('  =>'.green, info);
}

function convertToStr(buf, inputCharset) {
  let content = '';
  if (inputCharset && inputCharset !== 'utf-8') {
    content = iconv.decode(buf, inputCharset);
  } else if (buf) {
    content = buf.toString();
  }
  return content;
}

function getFileStat(filePath) {
  return new Promise((resolve) => {
    fs.stat(filePath, (err, stats) => {
      if (err) resolve();
      else resolve(stats);
    });
  });
}

function readFile(filePath) {
  return new Promise((resolve) => {
    fs.readFile(filePath, (err, data) => {
      if (err) resolve('');
      else resolve(data);
    });
  });
}

export default {
  trace,
  getExtName,
  comboAble,
  isTextFile,
  getAbsolutePath,
  isValidHost,
  dnsResolve,
  convertToStr,
  getFileStat,
  readFile
};
