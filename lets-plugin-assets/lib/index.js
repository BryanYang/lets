'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _startServer = require('./lib/startServer');

var _startServer2 = _interopRequireDefault(_startServer);

var _createApp = require('./lib/createApp');

var _createApp2 = _interopRequireDefault(_createApp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  name: 'assets',
  description: '启动 CDN 文件映射服务',
  options: [['-s, --sslPort <n>', 'SSL端口号（不指定则不监听https）', parseInt], ['-r, --root <s>', '指定某目录作为assets代理根目录'], ['-u, --useSrc', '指定asset代理使用src目录的资源'], ['-m, --sourcemap', '开启sourcemap'], ['-P, --port <n>', '端口号（默认80）', parseInt]],
  usage: '[options]',
  onHelp: '',
  action: function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(prog) {
      var app;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return (0, _createApp2.default)({});

            case 3:
              app = _context.sent;
              _context.next = 6;
              return (0, _startServer2.default)(app);

            case 6:
              return _context.abrupt('return', _context.sent);

            case 9:
              _context.prev = 9;
              _context.t0 = _context['catch'](0);

              console.error('\u542F\u52A8assets\u5931\u8D25: ' + _context.t0.message + '\n' + _context.t0.stack);
              throw _context.t0;

            case 13:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, this, [[0, 9]]);
    }));

    function action(_x) {
      return _ref.apply(this, arguments);
    }

    return action;
  }()
};