import http from 'http';

export default async function(app) {
  // let config = app.assetsConfig;
  return new Promise((resolve, reject) => {
  
    var server = http.createServer(app.callback())
      .listen('80', () => {
        resolve(server);
        console.log('服务启动80')
      })
      .on('error', e => {
          console.log(e);
          console.log(e.errno);
          reject(e);
          // reject(new Error(`HTTP服务器启动失败：端口80已被占用，请确认是否已经启动资源代理服务器`));
      });
  });
}
