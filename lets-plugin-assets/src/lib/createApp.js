import koa from 'koa';
import assets from 'lets-middleware-assets';

export default async function (config) {
  const app = new koa();
  app.assetsConfig = config;
  //app.use(join(config));
  app.use(assets(config));
  return app;
}
