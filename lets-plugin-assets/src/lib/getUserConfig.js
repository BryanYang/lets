import path from 'path';
import fs from 'fs';
let cache = {};

async function getConfigFromFile(source) {
  try {
    if (cache[source]) return cache[source];
    cache[source] = fs.existsSync(source) ? require(source) : {};
    return cache[source];
  } catch (e) {
    return {};
  }
}

export default async function(tempDir = '~/.tap', projectTapConfFile) {
  let userConfig = projectTapConfFile;
  let globalConfig = await getConfigFromFile(path.join(tempDir, 'tap.conf.js'));
  return {
    ...globalConfig,
    ...userConfig
  };
}
