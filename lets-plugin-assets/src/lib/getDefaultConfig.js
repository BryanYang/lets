import path from 'path';



/* 我自己写了一个映射
module.exports = {
    rule: '^g\\.alicdn\\.com\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)',
    path: '$1/$2/$3',
}
*/

export default async function(tempDir = '~/.lap') {
  return {
    urls: [{
      rule: /([\S]+?)\/([\S]+?)\/[\d\.]*/,   // url规则：group/仓库/版本号，例如g.tbcdn.cn/mui/test/1.2.3
      dest: path.join(tempDir, 'LocalCDNPath/$1/$2/')   // 本地目录规则：当前执行的路径，例如~/gitlab/mui/test/
    }],
    hosts: {},
    root: path.join(tempDir, 'LocalCDNPath')
  };
}
