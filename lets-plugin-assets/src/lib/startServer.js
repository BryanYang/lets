import startHttpServer from './startHttpServer';
// import startHttpsServer from './startHttpsServer';

export default async function(app) {
  return await Promise.all([startHttpServer(app)]);
}
