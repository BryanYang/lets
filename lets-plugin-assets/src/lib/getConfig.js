// 获取映射规则和路径

import _ from 'lodash';
import getUserConfig from './getUserConfig';
import getDefaultConfig from './getDefaultConfig';
import logger from './logger';
import track from './track';

export default async function(argv) {
  argv = {
    port: 80,
    ..._.pick(argv, ['port', 'sslPort', 'root', 'urls', 'hosts', 'useSrc', 'tempDir', 'projectTapConfFile', 'sourcemap'])
  };
  argv.sourcemap && track('sourcemap', 'assets');
  const userConfig = await getUserConfig(argv.tempDir, argv.projectTapConfFile);
 
  const defaultConfig = await getDefaultConfig(argv.tempDir);
  
  const userAssetsConfig = userConfig.assets || {};
  
  return _.merge({
    urls: [...(userAssetsConfig.urls || []), ...defaultConfig.urls],
    hosts: {
      ...defaultConfig.hosts,
      ...userAssetsConfig.hosts
    },
    root: userAssetsConfig.root || defaultConfig.root
  }, argv);
}
