import startServer from './lib/startServer';
import createApp from './lib/createApp';
import getConfig from './lib/getConfig';

module.exports = {
   name: 'assets',
   description: '启动 CDN 文件映射服务',
   options: [
      ['-s, --sslPort <n>', 'SSL端口号（不指定则不监听https）', parseInt],
      ['-r, --root <s>', '指定某目录作为assets代理根目录'],
      ['-u, --useSrc', '指定asset代理使用src目录的资源'],
      ['-m, --sourcemap', '开启sourcemap'],
      ['-P, --port <n>', '端口号（默认80）', parseInt],
   ],
   usage: '[options]',
   onHelp: '',
   action: async function(prog) {
      try {
        const config = await getConfig(prog);
        const app = await createApp(config);
        return await startServer(app);
      } catch(e) {
        console.error(`启动assets失败: ${e.message}\n${e.stack}`);
        throw e;
      }
    }
}