
* lets 工具自身目录, 不实现功能，只调用插件
* lets-plugin-assets， assets 的插件目录，该插件功能为静态代理脚本
* lets-middleware-assets，assets插件的中间件。

使用：

本地代理

```
lets assets -P 端口
```

本地初始化

```
lets init 脚手架名字
```
