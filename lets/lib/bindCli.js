'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

exports.default = bindCli;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PLUGIN_PREFIX = 'lets-plugin-';

function bindCli(program, argv) {

    var plugin = null;
    var sub_command = argv[2];

    try {
        var pluginName = '' + PLUGIN_PREFIX + sub_command;
        plugin = require(pluginName);
    } catch (e) {
        console.log(e.message);
    }

    if (!plugin) {
        return;
    }

    var command = plugin;
    var inst = program.command(command.name || cmd).description(command.description);

    (command.options || []).forEach(function (option) {
        inst.option.apply(inst, option);
    });

    inst.usage(command.usage || '[options]');

    inst.action(function () {
        var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
            var hasError,
                _len,
                args,
                _key,
                _args = arguments;

            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            hasError = false;
                            _context.prev = 1;

                            for (_len = _args.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                                args[_key] = _args[_key];
                            }

                            _context.next = 5;
                            return command.action.apply(this, args);

                        case 5:
                            _context.next = 10;
                            break;

                        case 7:
                            _context.prev = 7;
                            _context.t0 = _context['catch'](1);

                            hasError = true;

                        case 10:
                            hasError && process.exit(1);

                        case 11:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, this, [[1, 7]]);
        }));

        return function () {
            return _ref.apply(this, arguments);
        };
    }());
}
module.exports = exports['default'];