#!/usr/bin/env node
 
var {Command} = require('commander');
var bindCli = require('./bindCli');

var lets = {};

lets.run = async function(argv) {
  let program = new Command();
 
  program
  .option('-p, --peppers', 'Add peppers')
  

  await bindCli(program, argv);
  program.parse(process.argv);

}


 


module.exports = lets;
