const PLUGIN_PREFIX = 'lets-plugin-'

export default function bindCli(program, argv) {

    var plugin = null;
    const sub_command = argv[2];

    try{
        const pluginName = `${PLUGIN_PREFIX}${sub_command}`;
        plugin = require(pluginName);
    } catch(e) {
        console.log(e.message)
    }

    if(!plugin){
        return;
    }

    let command = plugin;
    let inst = program
        .command(command.name || cmd)
        .description(command.description);

    (command.options || []).forEach(option => {
        inst.option.apply(inst, option);
    });

    inst.usage(command.usage || '[options]');

    inst.action(async function(...args) {
        let hasError = false;
        try {
            await command.action.apply(this, args);
        } catch(e) {
            hasError = true;
        }
        hasError && process.exit(1);
    });

}